package EX4;

public class HomeAutomation {
    public static void main(String[] args){


        Home casa= new Home(){
            protected void setValueInEnvironment(Event event){
                System.out.println("New event in environment "+event);
            }
            protected void controlStep(){
                System.out.println("Control step executed");
            }
        };
        casa.simulate();
    }
}
