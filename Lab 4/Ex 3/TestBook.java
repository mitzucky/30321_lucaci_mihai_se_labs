package EX3;

import EX2.Author;

public class TestBook {
    public static void main(String[] args) {

        Author aut = new Author("Feodor Dostoievski", "Feodor@yahoo.com", 'm');
        Book cartea = new Book("Fratii Karamazov", aut, 9.99, 99);
        System.out.println("The name of the book is: " + cartea.getName());
        System.out.println("The price of the book is: " + cartea.getPrice() + " $");

        System.out.println("There are " + cartea.getQtyInStock()+ " books in stock");

        System.out.println("The author of the book is: " + cartea.getAuthor());

        System.out.println("Author's name is: " + cartea.getAuthor().getName());

        System.out.println("Author's email is: " + cartea.getAuthor().getEmail());

        System.out.println("Author's gender is: " + cartea.getAuthor().getGender());

    }
}
