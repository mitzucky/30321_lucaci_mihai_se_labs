package EX3;

import EX2.Author;

public class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock=0;

    public Book(String name, double price,Author author)
    {
        this.author=author;
        this.price=price;
        this.name=name;
    }

    public Book(String name, Author author, double price, int qtyInStock)
    {

        this.name = name;
        this.author=author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }


    public Author getAuthor() {
        return author;  // return member author, which is an instance of the class Author
    }


    public String getName() {
        return name;
    }


    public void setPrice(int price)
    {
        this.price=price;
    }


    public double getPrice() {
        return price;
    }


    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }


    public int getQtyInStock() {
        return qtyInStock;
    }


    public String toString() {
        return "'" + name + "' by " + author;  // author.toString()
    }
}
