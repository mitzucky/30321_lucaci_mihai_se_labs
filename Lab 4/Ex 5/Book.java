package EX5;

import EX2.Author;

public class Book  {
    String name;
    private Author[] authors=new Author[3];
    double price;
    int qtyInStock=0;


    public Book(String name, double price, int qtyInStock, Author[] authors) {
        this.name = name;
        this.price = price;
        this.qtyInStock = qtyInStock;
        this.authors=authors;
    }

    public String getName(){
        return this.name;
    }

    public Author[] getAuthors() {
        return this.authors;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double newPrice) {
        this.price = newPrice;
        return;
    }

    public int getQtyInStock() {
        return this.qtyInStock;
    }

    public void setQtyInStock(int qty) {
        this.qtyInStock = qty;
    }

    public String toString() {
        String ret = "The books name is " + this.name + "' by " + authors.length + " author";
        return ret;
    }

    public void comma() {
        System.out.print(", ");
    }

    public void printAuthors() {
        String s = authors[0].getName();
        for (int i = 1; i < authors.length; ++i)
        {
            s = s + " , " + authors[i].getName();

        }

        System.out.println(s);

    }

}
