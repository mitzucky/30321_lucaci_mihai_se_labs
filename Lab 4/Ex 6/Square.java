package EX6;

public class Square extends Rectangle {
    public Square()
    {
        super();
    }

    public Square(double side)
    {
        super(side,side);

    }

    public Square (double side, String color, Boolean filled)
    {
        super(side,side,color,filled);

    }

    public double getSide() { return this.width; }


    public void setLength(double side) {this.length=side;
                                          this.width=side;}

     public void setWidth (double side) {this.width=side;
                                          this.length=side;}
    public String toString(){return "side: " + this.getSide() + ", color: " + this.color + ", filled: " + this.filled; }


}
