package EX6;

public class Test {
    public static void main(String[] args)
    {
        Shape shape = new Shape();
        Circle circle = new Circle();
        Rectangle rectangle = new Rectangle();
        Square square = new Square();

        System.out.println("shape: " + shape.toString() + '\n');

        System.out.println("circle:  area = " + circle.getArea() + "; perimeter = " + circle.getPerimeter());
        System.out.println(circle.toString() + '\n');

        System.out.println("rectangle: area = " + rectangle.getArea() + "; perimeter = " + rectangle.getPerimeter());
        System.out.println(rectangle.toString() + '\n');

        System.out.println("Square: area = " + square.getArea() + "; perimeter = " + square.getPerimeter());
        System.out.println(square.toString() + '\n');
    }
}
