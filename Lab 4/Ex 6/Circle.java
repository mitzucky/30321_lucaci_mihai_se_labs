package EX6;

public class Circle extends Shape {
    private double radius=1.0;

    public Circle() {
        radius = 1.0;
    }


    public Circle(double r) {
        radius = r;
    }

    public Circle(double r, String color, Boolean f)
    {
        radius=r;
        this.color=color;
        filled=f;

    }
    public void setRadius(double radius){this.radius=radius;}

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return radius*radius*Math.PI;
    }

    public double getPerimeter() {return 2*Math.PI*radius;}

    public String toString() {
        return "A circle with radius: " + radius + " which is a subclass of  " + super.toString();
    }

}
