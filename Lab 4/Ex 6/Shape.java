package EX6;

public class Shape {
    public String color="red";
    public Boolean filled=true;
    public Shape()
    {
        color="green";
        filled=true;

    }
    public Shape(String color, Boolean filled)
    {
        this.color=color;
        this.filled=filled;

    }
    public String getColor() {return this.color; }

    public Boolean FILLED() {return this.filled;}

    public void setColor(String color) { this.color=color;}

    public void setFiled(Boolean filled) {this.filled=filled;}

    public String toString() {
        return "A shape with color" + color + " and " + filled;
    }



}
