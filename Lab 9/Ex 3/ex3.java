package ex3;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;

class ButtonAndTextField2 extends JFrame{

    HashMap accounts = new HashMap();

    JLabel iText,fText;
    JTextField text1;
    JTextArea textf;
    JButton transfer;

    ButtonAndTextField2(){

        setTitle("Transfer text");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        iText = new JLabel("Scrieti ce doriti sa transferati ");
        iText.setBounds(10, 50, width, height);

        fText = new JLabel("Text Area: ");
        fText.setBounds(10, 100,width, height);

        text1 = new JTextField();
        text1.setBounds(70,50,width, height);



        transfer = new JButton("transfer");
        transfer.setBounds(10,150,width, height);

        transfer.addActionListener(new TransferText());

        textf = new JTextArea();
        textf.setBounds(10,180,150,80);

        add(iText);add(fText);add(text1);add(transfer);add(textf);

    }

    public static void main(String[] args) {
        new ButtonAndTextField2();
    }

    class TransferText implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            String usr = ButtonAndTextField2.this.text1.getText();
            ButtonAndTextField2.this.textf.append(usr);

        }
    }
}