package Ex5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class Simulator extends JFrame{

    JButton addButton = new JButton("Adaugare");
    JTextField trainID = new JTextField();
    JTextField trainSegment = new JTextField();
    JTextField trainDestination = new JTextField();
    static ArrayList<Segment> segments = new ArrayList<>();
    static ArrayList<Controler> controlers = new ArrayList<>();
    ArrayList<JTextArea> segmentAreas = new ArrayList<>();

    Simulator()
    {
        setTitle("Simulator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        display();
        setSize(400,320);
        setVisible(true);
        addButton.addActionListener(new Simulator.ButtonAction());
    }

    void display()
    {
        this.setLayout(new GridLayout(28,1));
        for(Controler controler : controlers)
        {
            addButton.setSize(50,20);
            ArrayList<Segment> segments = controler.list;
            JLabel controlerLabel = new JLabel();
            controlerLabel.setText(controler.stationName);
            add(controlerLabel);
            for(Segment segment : segments){
                JTextArea segmentArea = new JTextArea();
                segmentAreas.add(segmentArea);
                JLabel segmentLabel = new JLabel();
                segmentLabel.setText("Linia " + segment.id);
                try {
                    segmentArea.setText(segment.getTrain().name);
                }
                catch(Exception e) {
                    System.out.println("e");
                }
                add(segmentLabel); add(segmentArea);
            }
        }
        JLabel trainIDLabel = new JLabel("Numarul trenului");
        add(trainIDLabel); add(trainID);
        JLabel trainSegmentLabel = new JLabel("Linie");
        add(trainSegmentLabel); add(trainSegment);
        JLabel trainDestinationLabel = new JLabel("Destinatie");
        add(trainDestinationLabel); add(trainDestination);
        add(addButton);
    }

    class ButtonAction implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            Train t = new Train(trainDestination.getText(), trainID.getText());
            int segmentID = Integer.parseInt(trainSegment.getText());
            segmentAreas.get(segmentID - 1).append(" / " + t.name);
            for(Segment segment : segments){
                if(segment.id == segmentID){
                    segment.arriveTrain(t);
                    break;
                }
            }
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        Controler c1 = new Controler("Cluj-Napoca");
        controlers.add(c1);

        Segment s1 = new Segment(1);
        segments.add(s1);
        Segment s2 = new Segment(2);
        segments.add(s2);
        Segment s3 = new Segment(3);
        segments.add(s3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        //build station Bucuresti
        Controler c2 = new Controler("Bucuresti");
        controlers.add(c2);

        Segment s4 = new Segment(4);
        segments.add(s4);
        Segment s5 = new Segment(5);
        segments.add(s5);
        Segment s6 = new Segment(6);
        segments.add(s6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        //build station Constanta

        Controler c3 = new Controler("Constanta");
        controlers.add(c3);

        Segment s7 = new Segment(7);
        segments.add(s7);
        Segment s8 = new Segment(8);
        segments.add(s8);
        Segment s9 = new Segment(9);
        segments.add(s9);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);


        ArrayList<Controler> n1 = new ArrayList<Controler>();
        ArrayList<Controler> n2 = new ArrayList<Controler>();
        ArrayList<Controler> n3 = new ArrayList<Controler>();

        n1.add(c2);
        n1.add(c3);
        n2.add(c1);
        n2.add(c3);
        n3.add(c1);
        n3.add(c2);
        //connect the 2 controllers

        c1.setNeighbourController(n1);
        c2.setNeighbourController(n2);
        c3.setNeighbourController(n3);
        //testing
        Train t1 = new Train("Bucuresti", "IC-001");
        s1.arriveTrain(t1);

        Train t2 = new Train("Constanta","IR-3471");
        s5.arriveTrain(t2);

        Train t3 = new Train("Cluj-Napoca","R-202");
        s7.arriveTrain(t3);

        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();
        System.out.println("\nStart train control\n");

        //execute 3 times controller steps
        for(int i = 0;i<3;i++){
            System.out.println("### Step "+i+" ###");
            c1.controlStep();
            c2.controlStep();
            c3.controlStep();
            System.out.println();

            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
        }

        new Simulator();
    }
}
