package Ex5;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;

class Controler extends JFrame {

    String stationName;

    ArrayList<Controler> neighbourControllers = new ArrayList<>();

    ArrayList<Segment> list = new ArrayList<>();


    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(ArrayList<Controler> v){
        neighbourControllers = v;
    }

    void addControlledSegment(Segment s){
        list.add(s);
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId(){
        for(Segment s:list){
            if(!s.hasTrain())
                return s.id;
        }
        return -1;
    }

    void controlStep(){
        //check which train must be sent
        for(Segment segment:list){

            if(segment.hasTrain()){
                Train t = segment.getTrain();
                for(Controler neighbourController: neighbourControllers) {
                    if (t.getDestination().equals(neighbourController.stationName)) {
                        //check if there is a free segment
                        int id = neighbourController.getFreeSegmentId();
                        if (id == -1) {
                            System.out.println("Trenul +" + t.name + "din gara " + stationName + " nu poate fi trimis catre " + neighbourController.stationName + ". Nici un segment disponibil!");
                            return;
                        }
                        //send train
                        System.out.println("Trenul " + t.name + " pleaca din gara " + stationName + " spre gara " + neighbourController.stationName);
                        segment.departTRain();
                        neighbourController.arriveTrain(t, id);
                    }
                }
            }
        }//.for
    }//.


    public void arriveTrain(Train t, int idSegment)
    {
        for(Segment segment:list){
            if(segment.id == idSegment)
                if(segment.hasTrain()){
                    System.out.println("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
                    return;
                }else{
                    System.out.println("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }
        System.out.println("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");

    }

    public HashMap<Integer, String> displayStationState()
    {
        System.out.println("=== STATION "+stationName+" ===");
        HashMap<Integer, String> segmentMap = new HashMap<>();
        for(Segment s:list){
            if(s.hasTrain()) {
                segmentMap.put(s.id, s.getTrain().name);
                System.out.println("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|");
            }
            else
                System.out.println("|----------ID="+s.id+"__Train=______ catre ________----------|");
        }
        return segmentMap;
    }

}
