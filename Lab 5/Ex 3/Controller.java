package EX3;

public class Controller {
    private TemperatureSensor tempSensor = new TemperatureSensor();
    private LightSensor lightSensor = new LightSensor();

    public void control() {
        for(int i = 1;i <=20;i++){
            System.out.println("Temp:\n"+tempSensor.readValue());
            System.out.println("Light:\n"+lightSensor.readValue());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
