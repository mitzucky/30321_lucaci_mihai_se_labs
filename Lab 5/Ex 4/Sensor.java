package EX4;

public abstract class Sensor {
    protected String location="living room";
    public String getLocation()
    {
        return location;
    }
    public abstract int readValue();
}
