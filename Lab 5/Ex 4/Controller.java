package EX4;

public class Controller {

    private TemperatureSensor temp= new TemperatureSensor();
    private LightSensor light = new LightSensor();
    private static Controller controller;
    private Controller(){

    }
    public static Controller getController(){
        if(controller == null){
            controller = new Controller();
        }return controller;
    }

    public void control() {
        for(int i = 1;i <=20;i++){
            System.out.println("Temperature: \n"+temp.readValue());
            System.out.println("Light: \n"+light.readValue());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
