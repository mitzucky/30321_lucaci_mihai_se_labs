package EX1;

public class Square extends Rectangle {
    protected double side;
    Square()
    {
        side = super.getLength();
    }
    Square(double side)
    {
        this.side = super.getLength();
        this.side = side;
        super.setLength(side);
        super.setWidth(side);
    }
    Square(double side, String color, boolean filled) {
        this.side = super.getLength();
        this.side = side;
        super.setColor(color);
        super.setFilled(filled);
        super.setLength(side);
        super.setWidth(side);
    }
    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    //@Override
    public void setLength(double length) {
        super.setLength(length);
    }

    //@Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public String toString() {
        String s;
        if (super.isFilled()) {
            s = "filled";
        } else {
            s = "not filled";
        }
        return "A square with color " + super.getColor() + " and " + s;
    }
}
