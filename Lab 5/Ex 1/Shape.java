package EX1;

public abstract class Shape {
    protected String color ;
    protected boolean filled ;

    Shape() {
        this.color="red";
        this.filled = true;
    }

    Shape(String color, boolean filled) {
        this.color = color;
        this.filled=filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        String s;
        if(this.isFilled()) {s = "filled";}
        else {s= "not filled";}
        return "A shape with color " + this.getColor() + " and " + s ;
    }

    public abstract double getArea();

    public abstract double getPerimeter();
}

