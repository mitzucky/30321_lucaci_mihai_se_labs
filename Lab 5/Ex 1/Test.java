package EX1;

public class Test {
    public static void main(String[] args) {
        Shape[] testShapes = new Shape[3];
        testShapes[0] = new Circle(4, "brown", true);
        testShapes[1] = new Rectangle(3, 4, "green", false);
        testShapes[2] = new Square(5, "yellow", false);
        for (int i = 0; i < 3; i++) {
            System.out.println(testShapes[i].toString());
            System.out.println("with area: " + testShapes[i].getArea());
            System.out.println("and with perimeter: " + testShapes[i].getPerimeter());
        }
    }
}
