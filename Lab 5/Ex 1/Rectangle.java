package EX1;

public class Rectangle extends Shape {
    protected double  width;
    protected double length;

    Rectangle()
    {
        this.width=1.0;
        this.length=1.0;
    }

    Rectangle(double width, double length)
    {
        this.width=width;
        this.length=length;

    }

    Rectangle(double width, double length, String color, Boolean filled)
    {
        this.width = width;
        this.length = length;
        super.setColor(color);
        super.setFilled(filled);
    }
   public double getWidth() { return this.width;}

   public void setWidth(double width) {this.width=width;}

   public double getLength() {return this.length;}

   public void setLength(double length) {this.length=length;}

   public double getArea()
   {
       return this.length*this.width;
   }
   public double getPerimeter()
   {
       return 2*this.length+2*this.width;
   }

   @Override
    public String toString()
   {
       String s;
       if(super.isFilled())
       {
           s="filled";
       }
       else
           s="not filled";
       return "A rectangle with color " + super.getColor() + " and " + s;
   }

}
