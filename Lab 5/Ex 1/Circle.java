package EX1;

public  class Circle extends Shape {
    protected double radius;
    Circle()
    {
       this.radius=1;
    }
    Circle(double radius)
    {
        this.radius=radius;
    }
    Circle(double radius,String color,Boolean filled)
    {
        this.radius=radius;
        super.setColor(color);
        super.setFilled(filled);

    }
    public double getRadius()
    {
        return radius;
    }

    public void setRadius(double radius)
    {
        this.radius=radius;
    }

    public double getArea()
    {
        return this.radius * this.radius * Math.PI;
    }
    public  double getPerimeter()
    {
        return 2 * this.radius * Math.PI;
    }

    @Override
    public String toString() {
        String s;
        if (super.isFilled()) {
            s = "filled";
        } else {
            s = "not filled";
        }
        return "A circle with color " + super.getColor() + " and " + s;
    }


}
