package EX2;

public class ProxyImage implements Image {
    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private boolean rot;

    public ProxyImage(String fileName, boolean rot){
        this.fileName = fileName;
        this.rot=rot;
    }

    @Override

    public void display() {
        if(this.rot==false){
            if(realImage == null){
                realImage = new RealImage(fileName);
            }
            realImage.display();
        }
        else
        {
            if(rotatedImage == null){
                rotatedImage = new RotatedImage(fileName);
            }
            rotatedImage.display();
        }

    }
}
