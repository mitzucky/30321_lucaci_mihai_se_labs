package EX2;

public class Test {
    public static void main(String[] args) {
        ProxyImage img1= new ProxyImage("lab5",true);
        img1.display();
        ProxyImage img2= new ProxyImage("lab5",false);
        img2.display();
    }
}
