package main;

import entities.Student;
import faculties.Faculty;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("The user can do the following actions using these keys: 0-5 ");

        Faculty faculty = new Faculty();

        int i = scanner.nextInt();
        int check;
        check = 0;
        for(int k = 0; k < 2; k++) {
            System.out.println("0. Exit the program");
            System.out.println("1. Add a new student in faculty");
            System.out.println("2. Delete a student in faculty");
            System.out.println("3. Edit an existing student in faculty");
            System.out.println("4. Send a student to study");
            System.out.println("5. Send a student to take the exam");
            switch (i) {
                case 0: {
                    check = 1;
                    break;
                }
                case 1: {
                    System.out.println("Give an id");
                    int id = scanner.nextInt();
                    System.out.println("Give a name");
                    String name = scanner.next();
                    System.out.println("Give an address");
                    String address = scanner.next();
                    System.out.println("Say if the student is studying");
                    boolean studying = scanner.nextBoolean();
                    System.out.println("give a faculty id");
                    int facultyid = scanner.nextInt();
                    Student student = new Student(id, name, address, studying, facultyid);
                    faculty.addStudent(student);
                    break;
                }
                case 2: {
                    System.out.println("Give an id");
                    int id = scanner.nextInt();
                    faculty.removeStudent(id);
                    break;
                }
            }
            if(check == 1){
                break;
            }
            k--;
        }

    }

}
