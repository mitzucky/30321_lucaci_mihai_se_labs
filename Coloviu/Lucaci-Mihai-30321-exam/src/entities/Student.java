package entities;

import actions.ExamDay;
import actions.StudyDay;

public class Student extends Person implements StudyDay, ExamDay {

    private boolean studying;
    private int facultyid;

    public Student(int id, String name, String address, boolean studying, int facultyid){
        super(id, name, address);
        this.facultyid = facultyid;
        this.studying = studying;
    }

    public boolean isStudying() {
        return studying;
    }

    public int getFacultyid() {
        return facultyid;
    }

    public void study(){
        if(studying) {
            System.out.println("Student " + getName() + " is already studying");
        }else{
            studying = true;
            System.out.println("Student " + getName() + " was sent to study");
        }
    }

    public void takeExam(){
        if(studying){
            System.out.println("Student " + getName() + " passed");
        }else{
            System.out.println("Student " + getName() + " failed");
        }
    }

}
