package entities;

import entities.Student;

public class ArchitectureStudent extends Student {

    public ArchitectureStudent(int id, String name, String address, boolean studying, int facultyid) {
        super(id, name, address, studying, facultyid);
    }


    @Override
    public void study() {
        super.study();
        System.out.print(" at Architecture Faculty.");
    }

    @Override
    public void takeExam() {
        super.takeExam();
        System.out.println(" at Architecture Faculty.");
    }
}
