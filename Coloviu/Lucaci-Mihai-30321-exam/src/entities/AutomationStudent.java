package entities;

import entities.Student;

public class AutomationStudent extends Student {

    public AutomationStudent(int id, String name, String address, boolean studying, int facultyid) {
        super(id, name, address, studying, facultyid);
    }


    @Override
    public void study() {
        super.study();
        System.out.print(" at Automation Faculty.");
    }

    @Override
    public void takeExam() {
        super.takeExam();
        System.out.println(" at Automation Faculty.");
    }



}
