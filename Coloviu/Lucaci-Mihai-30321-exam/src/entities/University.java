package entities;

import faculties.ArchitectureAndUrbanPlanning;
import faculties.AutomationAndComputerScience;


public class University {

    protected String name, address;


    public University(String name, String address){
        this.name = name;
        this.address = address;
    }

    public void initFaculties(){
        AutomationAndComputerScience ac = new AutomationAndComputerScience();
        ArchitectureAndUrbanPlanning arch = new ArchitectureAndUrbanPlanning();
    }

}
