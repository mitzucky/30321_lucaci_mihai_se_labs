package examen2;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class GUI extends JFrame {
	private JTextField textField;
	private String textName;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s="name.txt";
		GUI a =new GUI(s);
		a.setVisible(true);

	}
	public GUI(String s) {
		getContentPane().setLayout(null);
		setBounds(0, 0, 300, 300);
		
		textName=s;
		JButton btnNewButton = new JButton("Write");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				FileWriter writer = new FileWriter(textName, false);
                
                writer.write(textField.getText()); 
          
                writer.close();
				} catch (IOException f) {
                    f.printStackTrace();
                }
				
			}
		});
		btnNewButton.setBounds(47, 39, 89, 23);
		getContentPane().add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(47, 73, 195, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
	}
}
