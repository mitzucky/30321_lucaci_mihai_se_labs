package EX4;

import java.io.Serializable;

public class Car implements Serializable {

    private String model;
    private double price;

    public Car(String model)
    {
        this.model = model;
        this.price = Math.round(((double)(Math.random()*100)));
    }
    public void move(){
        System.out.println("The car is moving"+this);
    }
    public String toString(){
        return "This car's model is " + model + " and it's price is " + price;
    }

}
