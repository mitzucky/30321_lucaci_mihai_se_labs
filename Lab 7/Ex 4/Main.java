package EX4;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Creation Create = new Creation();
        Car car =Create.createCar("Tesla");
        Car car1 = Create.createCar("Mustang");
        Car car2 = Create.createCar("Mercedes");
        Create.freezCar(car,"car.dat");
        Create.freezCar(car1,"car1.dat");
        Create.freezCar(car2,"car2.dat");
        Car x = Create.unfreezCar("car.dat");
        Car y = Create.unfreezCar("car1.dat");
        Car z = Create.unfreezCar("car2.dat");
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
    }
}