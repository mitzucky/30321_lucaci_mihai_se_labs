package EX4;

import java.io.*;

public class Creation {
    public Car createCar(String model){
        Car car = new Car(model);
        System.out.println(model + " is created.");
        return car;
    }
    void freezCar(Car car,String storeRecipientName) throws IOException
    {
        ObjectOutputStream object=new ObjectOutputStream( new FileOutputStream(storeRecipientName));
        object.writeObject(car);
        System.out.println(car+" : Will be in stock shortly");
    }
    Car  unfreezCar (String storeRecipientName)throws  IOException,ClassNotFoundException
    {
        ObjectInputStream inobject=new ObjectInputStream(new FileInputStream(storeRecipientName));
        Car car=(Car) inobject.readObject();
        System.out.println(car+" :is in stock again.");
        return car;
    }
}
