package EX3;

import java.io.*;
import java.util.Scanner;

public class DecryptionEncryption {
    public static void main(String[] args) throws Exception
    {
        int option;
        System.out.println("1. Encryption \n 2. Decryption \n");
        Scanner scanner = new Scanner(System.in);
        option = scanner.nextInt();
        switch(option){
            case 1:{
                try{
                    BufferedReader inStream = new BufferedReader(new FileReader("date.txt"));
                    BufferedWriter outStream = new BufferedWriter(new FileWriter("date.enc"));
                    String line = "";
                    while ( (line = inStream.readLine()) != null ) {
                        String toWrite = "";
                        for(int i=0; i!=line.length();i++) {
                            char c = line.charAt(i);
                            if(true) {
                                c++;
                                toWrite += c;
                            }
                        }
                        outStream.write(toWrite);
                        outStream.newLine();
                    }
                    inStream.close();
                    outStream.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
                break;
            }
            case 2:{
                try{
                    FileReader inStream1 = new FileReader("date.enc");
                    BufferedReader bufferedReader = new BufferedReader(inStream1);
                    FileWriter outstream1 = new FileWriter("date2.dec");
                    BufferedWriter bufferedWriter = new BufferedWriter(outstream1);
                    String line1 = "";
                    while ( (line1 = bufferedReader.readLine()) != null ) {
                        String toWrite = "";

                        for(int i=0; i!=line1.length();i++) {

                            char c = line1.charAt(i);
                            if(true){
                                c--;
                                toWrite += c;
                            }

                        }
                        bufferedWriter.write(toWrite);
                        bufferedWriter.newLine();
                    }
                    bufferedReader.close();
                    bufferedWriter.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;
            }
            default: System.out.println("Choose 1 or 2");
        }

    }
}
