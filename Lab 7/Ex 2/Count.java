package EX2;
import java.io.*;
import java.util.Scanner;

public class Count {
    public static void main(String[] args) throws IOException
    {
        File file = new File("date.txt");
        FileInputStream fileStream = new FileInputStream(file);
        InputStreamReader input = new InputStreamReader(fileStream);
        BufferedReader reader = new BufferedReader(input);
        String line;
        System.out.println("The character you are looking for is: ");
        Scanner scanner = new Scanner(System.in);
        String character = scanner.nextLine();
        int cont = 0;
        while((line = reader.readLine()) != null)
        {
            for(int i = 0; i<line.length(); i++){
                if(line.charAt(i) == character.charAt(0))
                {
                    cont++ ;
                }
            }
        }
        System.out.println("The number of occurrences of "+character+" int the text is = " + cont);
    }
}
