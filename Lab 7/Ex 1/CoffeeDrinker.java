package EX1;

public class CoffeeDrinker {
    void drinkCofee(Coffee c) throws TemperatureException, ConcentrationException,NumberException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Coffee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Coffee concentration to high!");
        if(c.getNumber()>2)
            throw new NumberException(c.getNumber(), "Too many coffees were made!");
        System.out.println("Drink coffee:"+c);
    }
}
