package Ex3;

public class Author {
    public String email;
    public String name;
    public char gender;

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public char getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String toString() {
        return name + " (" + gender + ") at " + email;
    }
    public static void main(String[] args)
    {
        Author testauthor;
        testauthor=new Author("Szoke-Manea Alexandru" ,"alexmanea2201@gmail.com",
                'm');
        System.out.println(testauthor.toString());

    }
}



