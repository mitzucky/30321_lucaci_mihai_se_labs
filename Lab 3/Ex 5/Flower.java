package Ex5;

public class Flower {
    int petal;
    static int count=0;
    Flower(int p){
        petal=p;
        count++;
        System.out.println("New flower has been created!");
    }
   public static void counting()
    {
        System.out.println("Sunt "+count+" obiecte");
    }

    public static void main(String[] args) {
        Flower f1 = new Flower(4);
        Flower f2 = new Flower(6);
        System.out.println("Sunt "+count+" obiecte");
        Flower.counting();

    }
}
