package Ex4;

public class MyPoint{
    private int x;
    private int y;
     public MyPoint(){
        x = 0;
        y = 0;
    }
    public MyPoint(int x, int y){
        this.x = x;
        this.y = y;
    }
    public int getterX()
    {
        return x;
    }

    public void setterX(int x)
    {
        this.x = x;
    }

    public int getterY()
    {
        return y;
    }

    public void setterY(int y)
    {
        this.y = y;
    }
   public void SetXY(int a,int b)
   {
       x=a;
       y=b;
   }

   public void ToString() {
       System.out.println( "(" + x + ", " + y + ")");
   }

   public void distance(int x,int y)
   {
       int xd = this.x - x;
       int yd = this.y - y;
       double m= Math.sqrt(xd*xd + yd*yd);
       System.out.print(m+"\n");
   }
    public void distance(MyPoint another)
    {
        int xd = this.x - another.x;
        int yd = this.y - another.y;
        double m= Math.sqrt(xd*xd + yd*yd);
        System.out.print(m+ "\n");
    }


    public static void main(String[] args)
    {
        MyPoint TestMypoint = new MyPoint ();
        MyPoint TestMypoint2 = new MyPoint ();
        TestMypoint2.SetXY(2,2);
        TestMypoint.ToString();
        TestMypoint.SetXY(4,2);
        TestMypoint.ToString();
        TestMypoint.distance(TestMypoint2);
        TestMypoint.distance(1,1);




    }



}
