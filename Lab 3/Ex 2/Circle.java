package Ex2;

public class Circle {
    private double radius=1.0;
    private String colour="red";

    public double getRadius() {return radius;}
    public String getColour() {return colour;}

    Circle(double x) {radius=x;}
    Circle(String y) {colour=y;}
    public static void main(String[] args)
    {
        Circle TestCircle= new Circle( "Blue");
        System.out.println(TestCircle.getRadius());
        System.out.println(TestCircle.getColour());

    }

}
