package EX3;

public class BankAccount implements Comparable {
    private String owner;
    private double balance;

    BankAccount(){
        this.owner="Mihai";
        this.balance=110.22;
    }
    BankAccount(String owner,double balance){
        this.owner=owner;
        this.balance=balance;
    }

    public double getBalance(){
        return this.balance;
    }

    public String getOwner(){
        return this.owner;
    }

    public void withdraw( double amount) {
        this.balance=this.balance-amount;
    }

    public void deposit( double amount) {
        this.balance=this.balance+amount;
    }

    public boolean equals(BankAccount cont) {
        return cont.balance==this.balance && cont.owner==this.owner;
    }

    public int hashCode(){
        return owner.hashCode();
    }

    public int compareTo(Object o) {
        BankAccount p = (BankAccount)o;
        if(balance>p.balance) return 1;
        if(balance==p.balance) return 0;
        return -1;
    }
}
