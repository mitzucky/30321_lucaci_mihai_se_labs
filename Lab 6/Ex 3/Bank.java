package EX3;

import java.util.Comparator;
import java.util.TreeSet;

public class Bank {
    private TreeSet<BankAccount> lista=new TreeSet<BankAccount>();
    Comparator<BankAccount >compare=Comparator.comparing(BankAccount::getOwner);
    private TreeSet<BankAccount> lista2=new TreeSet<BankAccount>(compare);
    void addAccount(BankAccount cont){
        lista.add(cont);
        lista2.add(cont);
    }
    void printAccounts(){
        for (BankAccount value : lista)
            System.out.println(value.getOwner() + " "+ value.getBalance());

    }
    void printAccounts(double min, double max){
        for (BankAccount contafisat : lista)
            if(contafisat.getBalance()>=min && contafisat.getBalance()<=max)
                System.out.println(contafisat.getOwner()+ " "+contafisat.getBalance());
    }
    void getAccount(String proprietar){
        for (BankAccount value : lista)
            if(value.getOwner()==proprietar)
                System.out.println(value.getOwner() + " "+ value.getBalance());
    }
    void getAllAccounts(){
        for (BankAccount contafisat : lista2)
            System.out.println(contafisat.getOwner()+ " "+contafisat.getBalance());
    }
}
