package EX3;



public class Test {
    public static void main(String[] args) {
        Bank bank= new Bank();
        BankAccount b1= new BankAccount();
        BankAccount b2= new BankAccount("Alexandru",39.16);
        BankAccount b3= new BankAccount("Stefan",48.20);

        bank.addAccount(b1);
        bank.addAccount(b2);
        bank.addAccount(b3);

        System.out.println("Conturile sunt:");
        bank.printAccounts();

        System.out.println("Conturi in acel interval:");
        bank.printAccounts(20,40);
        System.out.println("Conturile in ordine alfabetica:");
        bank.getAllAccounts();
        System.out.println("Cautare  dupa nume dat:");
        String proprietar="Alexandru";
        bank.getAccount(proprietar);
    }
}
