package EX4;

import java.util.Scanner;

public class Menu {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        Dictionary dictionary = new Dictionary();
        int value;
        String line, explication;
        do {
            System.out.println("Menu");
            System.out.println("1 - Add a word and the definition");
            System.out.println("2 - Show all the words in the dictionary");
            System.out.println("3 - Show all the definitions in the dictionary");
            System.out.println("4 - Show the definition of a given word");
            System.out.println("5 - Exit");

            line = in.nextLine();
            value = line.charAt(0);

            switch (value)
            {
                case '1':
                {
                    System.out.println("Add a word: ");
                    line = in.nextLine();
                    Word word = new Word(line);
                    Definition definition = null;
                    if (line.length() > 1)
                    {
                        System.out.println("Add a definition:");
                        explication = in.nextLine();
                        definition = new Definition(explication);
                    }
                    dictionary.addWord(word, definition);
                } break;
                case '2':
                {
                    System.out.println("The words are: \n");
                    dictionary.getAllWords();
                } break;
                case '3':
                {
                    System.out.println("The definitions are:\n");
                    dictionary.getAllDefinitions();
                } break;
                case '4':
                {
                    System.out.println("Introduce a word");
                    line = in.nextLine();
                    Word word = new Word(line);
                    Definition definition = dictionary.getDefinition(word);
                    System.out.println("The definition of the word " + word + " is " + definition);
                } break;
            }
        } while (value != '5');
        System.out.println("Program finished.");
    }
}
