package EX4;

public class Definition {
    String description;

    public Definition(String description)
    {
        this.description = description;
    }

    public String toString()
    {
        return this.description;
    }
}
