package EX4;

import java.util.HashMap;
import java.util.Scanner;


public class Dictionary {
    HashMap<Word, Definition> dictionary = new HashMap<Word,Definition>();

    public void addWord(Word w,Definition d)
    {
        if(dictionary.containsKey(w))
        {
            System.out.println("Modifying the existent word");
        }
        else
        {
            System.out.println("New word added");
        }
        dictionary.put(w,d);
    }

    public Definition getDefinition (Word w)
    {
        Scanner in = new Scanner(System.in);

        if(dictionary.containsKey(w))
            return (Definition) dictionary.get(w);
        else
        {   System.out.println("This word and definition does not exist, please add something");
            String definitionAdded = in.nextLine();
            Definition definition = new Definition(definitionAdded);
            return definition;
        }
    }

    public void getAllWords()
    {
        if(dictionary.size() != 0)
            System.out.println(dictionary.keySet().toString());
        else
            System.out.println("Empty dictionary");
    }

    public void getAllDefinitions()
    {
        if(dictionary.size()!=0)
            System.out.println(dictionary.values().toString());
        else
            System.out.println("Empty dictionary");
    }
}
