package EX4;

public class Word {

    private String name;

    public Word(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (object == null || getClass() != object.getClass())
        {
            return false;
        }
        Word x = (Word) object;
        return x.name.equals(name);
    }

    public int hashCode()
    {
        return  name.hashCode();
    }
}
