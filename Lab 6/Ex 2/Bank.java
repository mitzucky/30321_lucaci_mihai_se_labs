package EX2;

import java.util.*;
public class Bank {

        private ArrayList<BankAccount> lista=new ArrayList<BankAccount>();
        void addAccount(BankAccount cont){
            lista.add(cont);
        }
        void printAccounts(){
            lista.sort(Comparator.comparing(BankAccount::getBalance));
            for(int i=0;i<lista.size();i++){
                BankAccount contafisat = lista.get(i);
                System.out.println(contafisat.getOwner()+ " "+contafisat.getBalance());
            }
        }
        void printAccounts(double min, double max){
            lista.sort(Comparator.comparing(BankAccount::getBalance));
            for(int i=0;i<lista.size();i++){
                BankAccount contafisat = lista.get(i);
                if(contafisat.getBalance()>=min&&contafisat.getBalance()<=max)
                    System.out.println(contafisat.getOwner()+ " "+contafisat.getBalance());
            }
        }
        void getAccount(String proprietar){
            for(int i=0;i<lista.size();i++){
                BankAccount contafisat = lista.get(i);
                if(contafisat.getOwner()==proprietar)
                    System.out.println(contafisat.getOwner()+ " "+contafisat.getBalance());
            }
        }
        void getAllAccounts(){
            lista.sort(Comparator.comparing(BankAccount::getOwner));
            for(int i=0;i<lista.size();i++){
                BankAccount contafisat = lista.get(i);
                System.out.println(contafisat.getOwner()+ " "+contafisat.getBalance());
            }
        }
}

