package EX2;

public class BankAccount {
    private String owner;
    private double balance;
    BankAccount(){
        this.owner="Adrian";
        this.balance=24.30;
    }
    BankAccount(String owner,double balance){
        this.owner=owner;
        this.balance=balance;
    }
    public double getBalance(){
        return this.balance;
    }

    public String getOwner(){
        return this.owner;
    }

    public void withdraw( double amount) {
        this.balance=this.balance-amount;
    }

    public void deposit( double amount) {
        this.balance=this.balance+amount;
    }

    public boolean equals(BankAccount cont) {
        return cont.balance==this.balance&&cont.owner==this.owner;
    }

    public int hashCode(){
        return owner.hashCode();
    }
}
