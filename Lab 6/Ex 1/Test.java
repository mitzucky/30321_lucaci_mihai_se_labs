package EX1;

public class Test {
    public static void main(String[] args)
    {
        BankAccount b1 = new BankAccount("Alex",25);
        BankAccount b2 = new BankAccount("Alexandru",40) ;
        BankAccount b3 = new BankAccount("Alexandru",45) ;

        b1.deposit(25);

        System.out.println("b1 == b2: " + b1.equals(b2));
        System.out.println("b3 == b2: " + b2.equals(b3));

        System.out.println("b1 hashcode is: " +  b1.hashCode());
        System.out.println("b2 hashcode is: "+ b2.hashCode());
        System.out.println("b3 hashcode is: "+  b3.hashCode());







    }
}
