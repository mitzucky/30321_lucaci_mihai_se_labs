package EX1;

public class BankAccount {
    public String owner;
    public double balance;

    BankAccount(String owner, double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }

    public void withdraw(double amount)
    {
          balance=balance-amount;
    }
    public void deposit(double amount)
    {
          balance=balance+amount;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof BankAccount){
            BankAccount p = (BankAccount) obj;
            return balance == p.balance;
        }
        return false;
    }

    public int hashCode(){
        int hash = (int)balance +(int)(100*(balance-(int)(balance))) + owner.hashCode();
        return hash;
    }
    public String toString(){
        return "The owner is: "+owner+":"+" and the balance is: "+balance;
    }
}
